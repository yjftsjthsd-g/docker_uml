# UML in docker

## Use

The closest I've gotten to making this work is:
```
docker run --network none --rm -v /dev/shm --tmpfs /dev/shm:rw,nosuid,nodev,exec registry.gitlab.com/yjftsjthsd-g/docker_uml:latest
```
I suspect that it would work if given a root filesystem, but I've not yet tested it.

